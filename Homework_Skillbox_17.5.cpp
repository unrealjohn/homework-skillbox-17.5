// Homework_Skillbox_17.5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <cmath>                      

class Vector
{
    public:
        Vector() 
        {}
        //Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
        //{}

        void Show()
        {
            std::cout << '\n' << "Vector values: " << x << ' ' << y << ' ' << z << '\n';
            
            //Calculating Vector length
            double Vector_Length = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
            std::cout << "Vector length: " << Vector_Length << '\n';
        }

    private:
        double x = 50;
        double y = -25;
        double z = -100;
};


int main()
{
    Vector v;
    v.Show();
}



